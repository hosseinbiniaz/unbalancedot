#!/usr/bin/env python3

from doctest import OutputChecker
import numpy as np
import singlescale_singlescalesolver as mod

from funcHeader import *

import time

distr = int(sys.argv[1]) if len(sys.argv) > 1 else 2
params = int(sys.argv[2]) if len(sys.argv) > 2 else 1
# ---------------------------------------

listOfOutputarrIs = [] 
listOfOutputarrJs = []
elapsed_time_s = []
# grep "final objective is:" for listing out final objectives 
# grep "counters: ---> " for listing the benchmark counters for failed/successful pushes/pulls
m=n=8
for _ in range(6):
    x = np.linspace(0, 1., num=m, endpoint=False)
    y = np.linspace(0, 1., num=n, endpoint=False)

    mu, nu = getDistr(distr, params,m,n, x)
    # mu = 1.5*m-np.arange(m)
    # mu = mu/np.sum(mu)
    # nu = 0.5*n+np.arange(n)  # self.nu=self.nu/np.sum(self.nu)
    # nu = nu/np.sum(nu)

    # multiscale(mu, nu, x, y)

    arrJ = np.full(shape=m-1, fill_value=0, dtype=np.int32)
    arrI = np.full(shape=n-1, fill_value=m-1, dtype=np.int32)

    start_time = time.time()

    hk1d_Obj = mod.hk1d(arrJ, arrI, mu, nu, x, y)

    elapsed_time_s.append(time.time() - start_time)

    listOfOutputarrIs.append(arrI)
    listOfOutputarrJs.append(arrJ)


    print("after iterations: ", arrI, arrJ)
    m*=2
    n*=2
print(f'list of arrIs for dim=8, 16, 32, 64, ...\
{listOfOutputarrIs}')
print('---------------------------')
print(f'list of arrJs for dim=8, 16, 32, 64, ...\
{listOfOutputarrJs}')

print("elapsed times ---> dim = 8, 16, ..., 2^9 ", elapsed_time_s)
# grep elapsed times ---> for the elapsed times

# final objective is: 0.97638154150414704
# [ 0  0  1  1  2  3  3  4  5  5  6  7  8  8  9 10 11 12 13 14 15 16 17 19
#  20 21 23 24 26 27 29]|[ 2  4  5  7  8 10 11 12 14 15 16 17 18 19 20 21 22 23 23 24 25 26 26 27
#  28 28 29 30 30 31 31]

# final objective is: 0.97651143587204723
# [ 0  0  1  1  2  2  3  3  4  4  5  6  6  7  8  8  9 10 10 11 12 12 13 14
#  15 16 16 17 18 19 20 21 22 23 24 24 25 26 27 29 30 31 32 33 34 35 36 38
#  39 40 41 43 44 46 47 49 50 52 53 55 57 59 61]|[ 2  4  6  8 10 11 13 14 16 17 19 20 22 23 24 25 27 28 29 30 31 32 33 34
#  36 37 38 39 39 40 41 42 43 44 45 46 47 47 48 49 50 51 51 52 53 53 54 55
#  55 56 57 57 58 59 59 60 60 61 61 62 62 63 63]