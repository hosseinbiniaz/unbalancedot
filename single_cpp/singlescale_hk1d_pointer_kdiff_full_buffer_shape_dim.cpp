#include <stdio.h>
#include <iostream>
#include <tuple>
#include <tgmath.h>
#include <pybind11/pybind11.h>
#include <chrono>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
#include <omp.h>

// compile:
// clang++ -shared -fPIC -std=c++11 -I./pybind11/include/ `python3.10 -m pybind11 --includes` singlescale_hk1d_pointer_kdiff_full_buffer_shape_dim.cpp -o singlescale_singlescalesolver.so `python3.10-config --ldflags`
// in python multiscale: import singlescale_pybind as singlescale


using namespace std;
namespace py = pybind11;
// create a class first 

// then debug

class hk1d 
{
  public: 
    bool didUpdate; 
    int m, n, k_A, k_B, i; 
    vector<long double> c; //self.c = self.getCost(self.x, self.y)
    long double* mu , *nu;
    long double Obj, A, B, I_kA, I_kB, II_kA, II_kB, III_kA, III_kB;
    double* x, *y;
    // vector<double> x, y;
    int* arrI,* arrJ;
    // vector<int> arrI, arrJ; 

    // counters for trying and failing or not going throuh due to not geometrically possible to push/pull
    size_t push_not_possible, push_failed, push_succeeded, pull_not_possible, pull_failed, pull_succeeded;

    // hk1d(vector<int> _arrJ, vector<int> _arrI, vector<long double> _mu, vector<long double> _nu, vector<double> _x, vector<double> _y)
    hk1d(int* _arrJ, int* _arrI, long double * _mu, long double * _nu, double * _x, double * _y, int xdim, int ydim)
    : didUpdate(false), m(xdim),n(ydim), i(0),y(_y), x(_x),  nu(_nu), mu(_mu), arrI(_arrI), arrJ(_arrJ)
    {   
        c = vector<long double>(m*n,0.0);
        getCost(x , y, m, n, c);
        // k_A = m-2 + 1; k_B = arrJ[k_A - 1]; if(m==4)k_B+=1;
        k_A = m-2 + 1;k_B = arrJ[k_A - 1] + 1;
        Obj = A = B = I_kA = I_kB = II_kA = II_kB = III_kA = III_kB = 0.0;
        push_not_possible = push_failed = push_succeeded = pull_not_possible = pull_failed = pull_succeeded = 0;
        // setter_DualObj(arrI, arrJ, m, n, cmu, nu, c, k_A, k_B, Obj, A, B, I_kA, I_kB, II_kA, II_kB, III_kA, III_kB);
        setter_DualObj(false);
    };


// A: 0.8623120511414315, B: 1.5336194422230993, I_kA, 0.7788007830714049, 
// I_kB: 1.0, II_kA: 0.767348247019611, II_kB: 0.14285714285714285, III_kA: 
// 0.16666666666666666, III_kB: 1.017503434365902

    // std::tuple<int, int, std::vector<int>, std::vector<int>, long double>  operator()();
    void operator()();
    void update_A_k(bool forward);
    void update_B_k(bool forward);
    std::tuple<long double,long double,long double> rec_DualObj(int tryk_A, int tryk_B, bool pullOrNot);
    bool TryPulling();
    bool TryPushing();
    void setter_DualObj(bool beta);
    // void setter_DualObj();
    long double getWeightH(int i, int j, int m, std::vector<long double>& c);
    long double getWeightV(int i, int j, int m, std::vector<long double>& c);
    void getCost(const double * x, const double * y, int m, int n, std::vector<long double>& cost);

    // next: implement try pushing and pulling + recdualobj
};



// std::tuple<int, int, std::vector<int>, std::vector<int>, long double>  hk1d::operator()()
void hk1d::operator()()
{  
    while (true)
    {
            // # print('\n\n', self.i, self.arrI, self.arrJ, self.c, '\n\n')
            bool didPush = false;
            bool res = false;
            res = TryPushing();
            if (res)
            {
                didUpdate = true;
                didPush = true;
                while(res) res=TryPushing();
            }
            if (!didPush)
            {
                res = TryPulling();
                if(res)
                {
                    didUpdate = true;
                    while (res) res = TryPulling();
                }
            }
            i += 1;
            if (i == m)
            {
                // print("completed {i} loop: ".format(i=i), Obj)
                if (!didUpdate)
                {
                    cout.precision(17);
                    cout << "final objective is: " << Obj;
                    cout << "counters: ---> dim = " << m << " ----> ";
                    cout << "  push_not_possible: " << push_not_possible;
                    cout << "  push_failed: " << push_failed;
                    cout << "  push_succeeded: " << push_succeeded;
                    cout << "  pull_not_possible: " << pull_not_possible;
                    cout << "  pull_failed: " << pull_failed;
                    cout << "  pull_succeeded: " << pull_succeeded << endl;
                    break;
                    // print(arrJ, arrI, Obj, sep='|', end='')
                    // return std::make_tuple(n, m, arrI, arrJ, Obj);
                }
                didUpdate = false;
                i = 0;
                if (m != 4){
                k_A = 1;
                k_B = arrJ[k_A - 1] + 1;
                cout << endl << endl << "K_B is: " << k_B << endl << endl;
                setter_DualObj(true);}
            }
    }
};


void hk1d::update_B_k(bool forward)
{

    // send pointer to I-III and change them
    // and then do the same for updatingB

    double s_k_B = exp(-getWeightH(arrI[k_B-1], k_B-1, m, c));
    if(forward == true)
    {
        
        double s_k_B_plus_1 = exp(-getWeightH(arrI[k_B], k_B, m, c));
        I_kB = I_kB * s_k_B;
        II_kB = II_kB + I_kB * nu[k_B];
        III_kB = (B - II_kB)/(I_kB * s_k_B_plus_1);

        k_B += 1;
    }
    else
    {
        double s_k_B_minus_1 = exp(-getWeightH(arrI[k_B-2], k_B-2, m, c));

        // order is important:
        II_kB = II_kB - I_kB * nu[k_B-1];
        I_kB = I_kB / s_k_B_minus_1;
        // # III_kB = III_kB * s_k_B + mu[k_A-1]
        III_kB = (B - II_kB)/(I_kB * s_k_B_minus_1);

        k_B -= 1;
    }

}

void hk1d::update_A_k(bool forward)
{

    // send pointer to I-III and change them
    // and then do the same for updatingB

    long double s_k_A = exp(-getWeightV(k_A-1, arrJ[k_A-1], m, c));
    
    if(forward){
        long double s_k_A_plus_1 = exp(-getWeightV(k_A, arrJ[k_A], m, c));

        I_kA = I_kA * s_k_A;
        II_kA = II_kA + I_kA * mu[k_A];
        III_kA = (A - II_kA)/(I_kA * s_k_A_plus_1);

        k_A += 1;
    }
    else
    {
        long double s_k_A_minus_1 = exp(-getWeightV((k_A)-2, arrJ[k_A-2], m, c));

        // order is important:
        II_kA = II_kA - I_kA * mu[k_A-1];
        I_kA = I_kA / s_k_A_minus_1;
        // III_kA = III_kA * s_k_A + mu[k_A-1]
        III_kA = (A - II_kA)/(I_kA * s_k_A_minus_1);

        k_A -= 1;
    };

};




bool hk1d::TryPushing()
{
    bool pushingPossible = false;
    if ((i > 0) && (i < m-1)) pushingPossible = (arrJ[i]-arrJ[i-1] > 0);
    else if (i == 0) pushingPossible = (arrJ[i] > 0);

    if (!pushingPossible) {push_not_possible+=1;return false; }
        
    int tryk_B = arrJ[i]-1+1;
    int tryk_A = i+1;

    // # newObj, A_new, B_new = self.recDualObj(k_A=k_A, k_B=k_B, pullOrNot=False)

    std::tuple<long double,long double,long double> res = rec_DualObj(tryk_A, tryk_B, false);
    long double newObj = get<0>(res);
    long double A_new = get<1>(res);
    long double B_new = get<2>(res);

    if (newObj < Obj){
        push_succeeded+=1;
        arrI[arrJ[i]-1] += 1;
        arrJ[i] -= 1;
        Obj = newObj;
        A = A_new;
        B = B_new;
        return true;
    } else {push_failed+=1;return false;}

};



bool hk1d::TryPulling()
{
    bool pullingPossible = false;
    if (i < m-2) pullingPossible = (arrJ[i+1]-arrJ[i] > 0);
    else if (i == m-2) pullingPossible = (n-1-arrJ[i] > 0);

    if (!pullingPossible) {pull_not_possible+=1;return false;}
    
    int tryk_B = arrJ[i]+1;
    int tryk_A = i+1;

//     std::tie(a, b) = std::make_tuple(2, 3);
//    // a is now 2, b is now 3
//    return a + b; // 5
    std::tuple<long double,long double,long double> res = rec_DualObj(tryk_A, tryk_B, true);
    long double newObj = get<0>(res);
    long double A_new = get<1>(res);
    long double B_new = get<2>(res);


    if (newObj < Obj){
        pull_succeeded+=1;
        arrI[arrJ[i]] -= 1;
        arrJ[i] += 1;
        
        Obj = newObj;
        A = A_new;
        B = B_new;
        return true;
    } else {pull_failed+=1;return false;}

};



std::tuple<long double,long double,long double> hk1d::rec_DualObj(int tryk_A, int tryk_B, bool pullOrNot)
{   
    if (I_kA==0) cout << "needs to be initialized" << endl;
 
    int diff_A = k_A - tryk_A; int diff_B = k_B - tryk_B;

    cout << "k_diff B: "<< diff_B << " A: "<< diff_A << endl;

    if (diff_A)
    {   
        cout << endl << "entering diff_A updates" << endl;
        for(int i=0; i < abs(diff_A); i++)
        {
            cout << endl << "self_kA:  " << k_A << endl;
            cout << endl << "I_kA:  " << I_kA << endl;
            cout << endl << "II_kA:  " << II_kA << endl;
            
            if (diff_A < 0) update_A_k(true);
            
            else if (diff_A > 0)update_A_k(false);
        }
    }  

    if (diff_B)
    {
        cout << "entering diff_B updates" << endl;
        for(int j=0; j < abs(diff_B); j++)
        {   
            cout << endl << "self_kB:  " << k_B << endl;
            if (diff_B < 0) update_B_k(true);
            
            else if (diff_B > 0)update_B_k(false);
        } 
    }  

    int _arrI_K_B_minus_1 = arrI[k_B-1];
    int _arrJ_K_A_minus_1 = arrJ[k_A-1];


    cout << endl << "before : " << _arrI_K_B_minus_1 << " and " <<  _arrJ_K_A_minus_1 << endl << endl;


    if (pullOrNot)
    {
        cout << endl << "adding to _arrI_K_B_minus_1" << endl << endl;
        _arrI_K_B_minus_1 -= 1;
        _arrJ_K_A_minus_1 += 1;
    }
    else
    {
        cout << endl << "subtracting to _arrI_K_B_minus_1" << endl << endl;
        _arrI_K_B_minus_1 += 1;
        _arrJ_K_A_minus_1 -= 1;
    }

    cout << endl << "after : " << _arrI_K_B_minus_1 << " and " <<  _arrJ_K_A_minus_1 << endl << endl;


    long double s_k_A_new = exp(getWeightV(k_A-1, _arrJ_K_A_minus_1, m, c ));
    long double s_k_B_new = exp(getWeightH(_arrI_K_B_minus_1, k_B-1, m, c ));

    cout << endl << "s_k_A_new : " << s_k_A_new << " s_k_B_new " <<  s_k_B_new << endl << endl;
    
    long double A_new = II_kA + I_kA * s_k_B_new * III_kA;
    long double B_new = II_kB + I_kB * s_k_A_new * III_kB;

// pulling and tryk_A is 3 and tryk_B is 1
// k_diff B: 0, A:0
// s_k_A_new: 0.8290291181804004 and s_k_B_new: 1.2062302494209807
// I_kA: 0.7788007830714049, I_kB: 1.0, II_kA: 0.767348247019611,            
// II_kB: 0.14285714285714285, III_kA: 0.16666666666666666,III_kB: 1.017503434365902, 
// A_new: 0.874956334757593 , and B_new 1.3702005642790291
// res was true PULLING and i = 2

    return make_tuple(A_new * B_new, A_new, B_new);
    // return make_tuple(A_new * B_new, A_new, B_new, I_kA, I_kB, II_kA, II_kB, III_kA, III_kB);
};




// long double hk1d::getWeightV(int i, int j, int m, const long double* c)
long double hk1d::getWeightV(int i, int j, int m, std::vector<long double>& c)
{   
    return c[(i + 1)*m + j] - c[i*m + j];
    // return c[i + 1][j] - c[i][j];
};

// long double hk1d::getWeightH(int i, int j, int m, const long double* c) 
long double hk1d::getWeightH(int i, int j, int m, std::vector<long double>& c) 
{//m is the width of the cost matrix
    return c[m*i + j + 1] - c[m*i + j];
    // return c[i][j + 1] - c[i][j];
};

void hk1d::getCost(const double* x, const double* y, int m, int n, std::vector<long double>& cost){
    for(int i=0; i<m; i++)
    {
        for(int j=0; j<n; j++)
        {
            cost[i*m + j] = pow((x[i]-y[j]),2);
        }
    }
}

void hk1d::setter_DualObj(bool beta=false)
{       
    // auto _mu = mu.unchecked<1>();
    // auto _nu = nu.unchecked<1>();
    // auto _arrI = arrI.unchecked<1>();
    // auto _arrJ = arrJ.unchecked<1>();
    // auto c_buff = c.request();
    // double* _c = (double*)_c_buff.ptr;

    cout << endl << endl << "K_B is in setter: " << k_B << endl << endl;
    
    long double A_temp = 0;
    // double A = 0;
    long double Afact = 1;
    I_kA = 0;
    // double II_kA = 0;
    // double III_kA = 0;

    if (k_A <= 0 || k_B <= 0)
    {
        cout << "k =" << k_A << "/" << k_B << " has no meaning" << endl;
    }

    if (k_A == 1)
    {
        I_kA = Afact;
        II_kA = Afact * mu[0];
    }

    // if its in the beta version and the I_k has been set, then dont run this
    if (!(beta && I_kA!=0))
    {
        for (int i=0 ; i < m ; i++)
        {   
            if (beta) A_temp += Afact * mu[i];
            else A += Afact * mu[i];
            
            if (i < m-1)
            {
                // cout << "i < m-1" << endl;
                Afact *= exp(-getWeightV(i, arrJ[i], m, c));
            }
                    
            if (I_kA == 0 && (i == (k_A-2)))
            {
                // cout << "I_kA == 0 && (i == (k_A-2))"<<endl;
                I_kA = Afact;
                if (beta) {II_kA = A_temp + Afact * mu[i+1]; break;}
                else II_kA = A + Afact * mu[i+1];
            }
        }
    }

    if (k_A == m-1){
        // cout << "k_A == m-1" << endl;
        III_kA = mu[m-1];
    } else {
        // cout << "else for k_A == m-1" << endl;
        long double s_kA = exp(-getWeightV(k_A-1, arrJ[k_A-1], m, c));
        III_kA = (A - II_kA)/(I_kA * s_kA);
    }
    

    // -------------------------------------------------------

    long double B_temp = 0;
    // double B = 0;
    // long double Bfact = 1;
    long double Bfact = exp(c[0]);
    I_kB = 0;
    // double II_kB = 0;
    // double III_kB = 0;

    if (k_B == 1)
    {
        I_kB = Bfact;
        II_kB = Bfact * nu[0];
    }

    if  (!(beta && I_kB!=0)) 
    {
        for (int j=0 ; j < m ; j++)
        {   
            if (beta) B_temp += Bfact * nu[j];
            else B += Bfact * nu[j];
            
            if (j < n-1)
            {
                Bfact *= exp(-getWeightH(arrI[j], j, m, c));
            }
                    
            if (I_kB == 0 && (j == (k_B-2)))
            {
                I_kB = Bfact;
                if (beta) {II_kB = B_temp + Bfact * nu[j+1]; break;}
                else II_kB = B + Bfact * nu[j+1];
            }
        }
    }
    if (k_B == n-1){
        III_kB = nu[n-1];
    } else {
        long double s_kB = exp(-getWeightH(arrI[k_B-1], k_B-1, m, c));
        III_kB = (B - II_kB)/(I_kB * s_kB);
    }

    // set objective
    Obj = A*B;
};


// void DualObj(py::array_t<int>  arrJ, py::array_t<int>  arrI, int m, int n, py::array_t<long double> mu, 
// py::array_t<long double> nu, py::array_t<double> x, py::array_t<double> y)
void DualObj(py::array_t<int>  arrJ, py::array_t<int>  arrI, py::array_t<long double> mu, 
py::array_t<long double> nu, py::array_t<double> x, py::array_t<double> y)
{   
    // /* Request a buffer descriptor from Python */
    // py::buffer_info info = b.request();https://pybind11.readthedocs.io/en/stable/advanced/pycpp/numpy.html
    auto _mu_buff = mu.request();
    long double* _mu = (long double*)_mu_buff.ptr;
    auto _nu_buff = nu.request();
    long double* _nu = (long double*)_nu_buff.ptr;
    
    // cout << "size is" << _mu_buff.ndim << endl; 
    cout << "size is" << _mu_buff.shape[0] << endl;
    cout << "size is" << typeid(_mu_buff.shape[0]).name() << endl; 
    // l is ssize_t 
    // m is size_t
    // i in int

    // can cast automatically to int to feed into the class 
    // ssize_t is size_t with -1 as error
    ssize_t xdim = _mu_buff.shape[0];
    ssize_t ydim = _nu_buff.shape[0];

    cout << "size is" << typeid(xdim).name() << endl; 

    auto _x_buff = x.request();
    double* _x = (double*)_x_buff.ptr;
    auto _y_buff = y.request();
    double* _y = (double*)_y_buff.ptr;

    auto _arrJ_buff = arrJ.request();
    int* _arrJ = (int*)_arrJ_buff.ptr;
    auto _arrI_buff = arrI.request();
    int* _arrI = (int*)_arrI_buff.ptr;




    hk1d myObj( _arrJ,  _arrI, _mu, _nu,  _x,  _y, xdim, ydim);

    // cout << myObj.arrI[5] << endl;

    myObj(); 

};

PYBIND11_MODULE(singlescale_singlescalesolver, m) {
    m.doc() = "pybind11 cpp dualobj; everything else is self contained. Just expose the function DualObj.";
    m.def("hk1d", &DualObj ,"dualobj test function");
}
// clang++ -shared -fPIC -std=c++11 -I./pybind11/include/ `python3.10 -m pybind11 --includes` pointer_singlescale.cpp -o singlescalesolver.so `python3.10-config --ldflags`












// int main() {



//     int m, n;

//     m=n=32;
    
//     // // initialize with appropriate vectors
//     // std::vector<int> _arrI{3,3,3}, _arrJ{0,0,0};

//     // std::vector<int> _arrI(n-1,m-1), _arrJ(m-1,0);

//     int* _arrI = new int[m-1];
//     int* _arrJ = new int[m-1];

//     for (int i=0; i<m-1; i++){
//         // cout << "i is " << i <<endl;
//         _arrI[i] = m-1;
//         _arrJ[i] = 0;
//     }

//     // // std::vector<long double> _mu{0.34090909,0.28030303,0.21969697,0.15909091}, _nu{0.15322581,0.21774194,0.28225806,0.34677419};
//     // // std::vector<double> _x{0.09375,0.34375,  .59375,0.84375}, _y{0.09375,0.34375,0.59375,0.84375};

//     // // std::vector<double> _x, _y;

//     double* _x = new double[m];
//     double* _y = new double[n];

//     double delta = (1.0)/(m);
//     for (int i=0; i<m; ++i){
//         _x[i]= i*delta;
//         _y[i]= i*delta;
//     }

//     long double * _mu = new long double[m];
//     long double * _nu = new long double[n];

//     for (int i=0; i<m; ++i){
//         _mu[i]=1.5*m-i;
//     }
//     int sum_of_elems = 0;
//     for (int i=0; i<m; i++) sum_of_elems += _mu[i];
//     for (int i=0; i<m; i++){
//         _mu[i] /= sum_of_elems;
//     }

//     for (int i=0; i<m; ++i){
//         _nu[i]=0.5*m+i;
//     }
//     sum_of_elems = 0;
//     for (int i=0; i<m; ++i) sum_of_elems += _nu[i];
//     for (int i=0; i<m; ++i){
//         _nu[i] /= sum_of_elems;
//     }



//     hk1d myObj( _arrJ,  _arrI, _mu, _nu,  _x,  _y, m);

//     cout << myObj.arrI[5] << endl;

//     myObj();

//     for (int i=0; i<m-1; i++){
//     cout << myObj.arrI[i] << ' ';
//     }
//     cout << "done";

//     cout << endl; 

//     for (int i=0; i<m-1; i++){
//     cout << myObj.arrJ[i] << ' ';
//     }
    

//     return 0;
// }




// output:
// 0 0 1 1 2 3 3 4 5 5 6 7 8 8 9 10 11 12 13 14 15 16 17 19 20 21 23 24 26 27 29 done
// 2 4 5 7 8 10 11 12 14 15 16 17 18 19 20 21 22 23 23 24 25 26 26 27 28 28 29 30 30 31 31


// compare to calc_submit...
// 0 0 1 1 2 3 3 4 5 5 6 7 8 8 9 10 11 12 13 14 15 16 17 19 20 21 23 24 26 27 29 
// 2 4 5 7 8 10 11 12 14 15 16 17 18 19 20 21 22 23 23 24 25 26 26 27 28 28 29 30 30 31 31 

// hossein@hossein-Latitude-7420:~/Downloads/gitlab-OT/USINGPOINTERS$ python3 pointer_multiscale.py | grep "K_B is"
// K_B is in setter: 1
// K_B is in setter: 2
// K_B is: 2
// K_B is in setter: 2
// K_B is in setter: 2
// K_B is: 3
// K_B is in setter: 3
// K_B is: 3
// K_B is in setter: 3
// K_B is in setter: 4
// K_B is: 3
// K_B is in setter: 3
// K_B is: 3
// K_B is in setter: 3