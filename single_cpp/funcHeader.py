#!/usr/bin/env python3

from cmath import inf
import matplotlib.pyplot as plt
import numpy as np

import os
import sys
import time

legend_dict = {}   

def getGaussian(mean,var,M,x):
    print(f'x is {x}')
    mu=np.array([np.exp(-(i-mean)**2/(2*var)) for i in x])
    return mu/np.sum(mu)

def getExponential(lmbda,M):
    mu=np.array([lmbda*np.exp(-lmbda*i) for i in range(M)])
    return mu/np.sum(mu)

def getPi(arrJ,muEff,nuEff):
    n=m=len(arrJ)+1
    i=j=0
    pi=np.zeros((m,n),dtype=np.double)
    muEffLeft=muEff.copy()
    nuEffLeft=nuEff.copy()
    while (i<m) and (j<n):
        leaveRow=False
        if i<m-1:
            leaveRow=(arrJ[i]==j)
        if leaveRow:
            # we leave this row now, so all mass left in muEffLeft[i] needs to go into this coupling entry
            pi[i,j]=muEffLeft[i]
            nuEffLeft[j]-=muEffLeft[i]
            muEffLeft[i]=0.
            i+=1
        else:
            # leave column
            pi[i,j]=nuEffLeft[j]
            muEffLeft[i]-=nuEffLeft[j]
            nuEffLeft[j]=0.
            j+=1
    return pi


def KL(a,b):
    # index of small elements of a or b
    idx_a = np.where(np.abs(a) > 1E-5)
    idx_b = np.where(np.abs(a) > 1E-5)
    if len(idx_a[0]) != len(a) or len(idx_b[0]) != len(b):
        a = a[idx_a]
        b = b[idx_a]
    return np.sum(a*np.log(a/b)-a+b)

# def KL(a,b):
#     print(f"a is {a} and b is {b}")
#     idx = np.where(b>1E-5)[0]
#     return np.sum(a[idx]*np.log(a[idx]/b[idx])-a[idx]+b[idx])

# def PrimalObj(pi,c,mu,nu):
#     return np.sum(pi*c)+KL(np.sum(pi,axis=1),mu)+KL(np.sum(pi,axis=0),nu)

def expandArrays(arrI_old, arrJ_old):
    '''input (ndarray, ndarray)'''
    arrI_old *= 2
    arrJ_old *= 2
    idx = np.where(arrJ_old == len(arrJ_old)*2)[0][0]
    arrI_old = np.repeat(arrI_old, 2)
    arrJ_old = np.repeat(arrJ_old, 2)
    arrJ_old[2*idx:] = len(arrJ_old)+1
    return np.append(arrI_old, arrI_old[-1]), np.append(arrJ_old, arrJ_old[-1])


def getDistr(distr,params,m,n, x):
    if distr == 1:
        print('distribution: np.arrange * 0.5 VS 1.5')
        scale1 = 1.5
        scale2 = 0.5
        if params == 2:
            scale1 += 1
            scale2 += 2
        mu = scale1*m-np.arange(m)
        nu = scale2*n+np.arange(n)  # self.nu=self.nu/np.sum(self.nu)
        mu = mu/np.sum(mu)
        nu = nu/np.sum(nu)
        
        legend_dict[1]=['','']
        
        return mu, nu
    if distr == 2:
        # mudistr = 'Normal(0.4,0.01)'# nudistr = 'Normal(0.8,0.01)'
        print('distribution: normal distr VS perturbed slightly (50% of std.)')
        mean1 = 0.3 * np.max(x)
        mean2 =0.6 * np.max(x)
        std = 0.003 * np.max(x)**2
        # std = 0.002
        mu = getGaussian(mean1,std ,m, x) #is not negative
        nu = getGaussian(mean2,std ,n, x) 
        
        if params == 2:
            mean1 *=2 
            mean2 *=2

        if params ==3: 
            mean1 *=3 
            mean2 *=3

        legend_dict[2]=[f'Normal({round(mean1,2)},{round(std,3)})',f'Normal({round(mean2,2)},{round(std,3)})']
        
        return mu, nu
    if distr == 3:
        print('distribution: normal BIMODAL VS pertubed slightly (50% of std.)')
        mean1 = 0.3 * np.max(x)
        mean2 =0.6 * np.max(x)
        # std = 0.002
        std = 0.003 * np.max(x)**2
        mu=getGaussian(mean1, std,m, x) + getGaussian(mean2, std,m,x)#is not negative
        nu=getGaussian(mean1*1.3, std,m, x) + getGaussian(mean2*0.7,std,m,x)
        
        legend_dict[3]=[f'N({round(mean1,2)},{round(std,3)})+N({mean2},{std})',f'N({round(mean1*1.3,2)},{std})+N({round(mean2*0.7,2)},{round(std,3)})']

        if params == 2:
            mean1 *=2 
            mean2 *=2

        if params ==3: 
            mean1 *=3 
            mean2 *=3

        return mu, nu
    if distr == 4:
        print('distribution: EXPONENTIAL')
        lmbda1 = 1.5
        lmbda2 = 0.5
        mu = getExponential(lmbda1,m)
        nu = getExponential(lmbda2,m)

        legend_dict[4]=[f'Exp({lmbda1})',f'Exp({lmbda2})']

        return mu, nu 

def getSigmaTau(obj, arrJ,arrI,c):
    m = len(arrJ)+1
    n = len(arrI)+1
    sigma=np.zeros(m,dtype=np.double)
    tau=np.zeros(n,dtype=np.double)
    for i in range(m-1):
        sigma[i+1]=getWeightV(obj,i,arrJ[i])+sigma[i]
    tau[0]=c[0,0]
    for j in range(n-1):
        tau[j+1]=getWeightH(obj,arrI[j],j)+tau[j]
    return sigma,tau

def getAlphaBeta(arrJ,arrI,obj,c):
    sigma,tau=getSigmaTau(obj, obj.arrJ,obj.arrI,c)
    A=np.sum(np.exp(-sigma)*obj.mu)
    B=np.sum(np.exp(-tau)*obj.nu)
    alpha0=0.5*np.log(A/B)
    alpha=sigma+alpha0
    beta=tau-alpha0
    return alpha,beta

def getWeightH(obj, i, j):
        return (obj.x[i]-obj.y[j+1])**2  - (obj.x[i]-obj.y[j])**2  

def getWeightV(obj, i, j):
        return (obj.x[i+1]-obj.y[j])**2  - (obj.x[i]-obj.y[j])**2  

