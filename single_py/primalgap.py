#!/usr/bin/env python3

from flycost_cleaned_rec_plus_old import *

def getCost(x, y, lmbda):
        return lmbda*(np.abs(x.reshape((-1, 1))-y.reshape((1, -1))))**2

print(f'lmbda is {lmbda}')

c = getCost(x= hk1d_Obj.x,y= hk1d_Obj.y, lmbda=lmbda)

print('\n\nprimal dual results grep -A 20')


alpha,beta=getAlphaBeta(arrJ,arrI, hk1d_Obj,c)
muEff=np.exp(-alpha)*mu
nuEff=np.exp(-beta)*nu
pi=getPi(arrJ,muEff,nuEff)
print("eff marginal sums: ",np.sum(muEff),np.sum(nuEff))
print("min pi entry:", np.min(pi))

pi[pi<0]=0
print("min pi entry:", np.min(pi))

def PrimalObj(pi):
    return np.sum(pi*c)+KL(np.sum(pi,axis=1),mu)+KL(np.sum(pi,axis=0),nu)

def DualObj():
    return np.sum(mu)+np.sum(nu)-np.sum(np.exp(-alpha)*mu)-np.sum(np.exp(-beta)*nu)

print(f"primal obj is {PrimalObj(pi)} and dual obj is {DualObj()}")