#!/usr/bin/env python3

from primalgap import *


plt.plot(hk1d_Obj.x,mu, c  = "red")
plt.plot(hk1d_Obj.x,muEff,ls="dashed", c="green" )
plt.plot(hk1d_Obj.y,nu,c="yellow")
plt.plot(hk1d_Obj.y,nuEff,ls="dashed", c="blue")

plt.xlabel("x"); plt.ylabel("P(x)")
# plt.title(f"Distr: {dict_distr[distr]} with params {dict_params[params]}")

# plt.title(f"Hard VS estimated soft-marginals {(cost_config + "euclidean cost, dim: "hk1d_Obj.m)}")

# plt.legend()

plt.savefig(str(distr)+'_distr_comparison.png')
plt.show()

print(f'sum of mus {np.sum(muEff)}')

