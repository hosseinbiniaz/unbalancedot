#importing just hk1d recursive-class from cleaned_rec_plus_old_code.py as single scale solver with recursive formulas for dual objective
#!/usr/bin/env python3

from cmath import inf
import matplotlib.pyplot as plt
import numpy as np

import os
import sys
import time

__all__ = [
    'inf',
    'plt',
    'np',
    'os',
    'sys',
    'time'
]

class hk1d:
    i = 0

    def __init__(self, arrJ, arrI, mu, nu, x, y):
        '''mu and nu are np.arrays consistenting of measures (marginals)
            x and y are point clouds in the form of np.arrays
            arrJ/arrI are rows/cols exiting each col/row respectively i.e. arrJ[0]=1 means exit row[0] at column[1]'''

        self.didUpdate = False
        self.n = len(mu)
        self.m = len(nu)
        self.x = x
        self.y = y
        self.mu = mu
        self.nu = nu
        self.arrJ = arrJ
        self.arrI = arrI
        # self.c = self.getCost(self.x, self.y)
        if self.m==4:
            self.k_A = self.m-2 + 1
            self.k_B = self.arrJ[self.k_A - 1] + 1
        else:
            self.k_A = 1
            self.k_B = self.arrJ[self.k_A - 1]
        print(f'self.k: A/B {self.k_A}, {self.k_B} \t dim is: {self.m}\n')
        self.Obj = self.SET_DualObj(self.arrJ, self.arrI, self.m, self.n)
        self.drawJI()
        self.push_not_possible = self.push_failed = self.push_succeeded = self.pull_not_possible = self.pull_failed = self.pull_succeeded = 0

    

    def SET_DualObj(self, _arrJ, _arrI, m, n, beta = False):
        A_temp=0
        Afact = 1.
        self.I_kA = 0
        if self.k_A <= 0 or self.k_B <= 0:
            print(f'"k={self.k_A}" has no meaning')
        if self.k_A == 1:
            self.I_kA = Afact
            self.II_kA = Afact*self.mu[0]
        
        if not (beta and self.I_kA):
            for i in range(m):
                A_temp += Afact*self.mu[i]
                if i < m-1:
                    Afact *= np.exp(-self.getWeightV(i, _arrJ[i]))
                if not self.I_kA and (i == (self.k_A-2)):
                    self.I_kA = Afact
                    self.II_kA = A_temp + Afact*self.mu[i+1]
                    if beta:
                        break
        
        if not beta:
            self.A = A_temp

        if self.k_A == m-1:
            self.III_kA = self.mu[m-1]
        else:
            s_kA = np.exp(-self.getWeightV(self.k_A-1, _arrJ[self.k_A-1]))
            self.III_kA = (self.A-self.II_kA)/(self.I_kA*s_kA)

        # ----------------------------------------------------------------

        B_temp = 0
        Bfact = np.exp(-self.lazy_getCost(0, 0))  # = S_0
        self.I_kB = 0
        if self.k_B == 1:
            self.I_kB = Bfact
            self.II_kB = Bfact*self.nu[0]
        if not (beta and self.I_kB):        
            for j in range(n):
                B_temp += Bfact*self.nu[j]
                if j < n-1:
                    Bfact *= np.exp(-self.getWeightH(_arrI[j], j))
                if not self.I_kB and (j == (self.k_B - 2)):
                    self.I_kB = Bfact
                    self.II_kB = B_temp + Bfact*self.nu[j+1]
                    if beta:
                        break
        if not beta: 
            self.B = B_temp

        if self.k_B == m-1:
            self.III_kB = self.nu[n-1]
        else:
            s_kB = np.exp(-self.getWeightH(_arrI[self.k_B-1], self.k_B-1))
            self.III_kB = (self.B-self.II_kB)/(self.I_kB*s_kB)

        return self.A*self.B


    def lazy_getCost(self, i, j):
        # return (np.abs(x.reshape((-1, 1))-y.reshape((1, -1))))**2
        return (self.x[i]-self.y[j])**2      

    def getWeightV(self, i, j):
        return self.lazy_getCost(i+1, j)-self.lazy_getCost(i, j)

    def getWeightH(self, i, j):
        return self.lazy_getCost(i, j+1)-self.lazy_getCost(i, j)

    def recDualObj(self, k_A, k_B, pullOrNot):
        # pull=True:
        # arrI[k_B-1]-=1 would affect s_{k_B + 1} so i must index k from zero
        # arrJ[k_A-1]+=1

        # pull=False:
        # arrI[k_B-1]+=1
        # arrJ[k_A-1]-=1

        if not self.I_kA:
            print('needs to be initialized')

        # first update the precomps self.I-III_K_A/B
        diff_A = self.k_A - k_A
        diff_B = self.k_B - k_B

        print(f'k_diff B: {diff_B}, A:{diff_A}')

        if diff_A:  # if diff_A = 0, no need for updates
            for i in range(np.abs(diff_A)):
                if diff_A < 0:  # k>self.k
                    self.update_A_k(forward=True)
                if diff_A > 0:
                    self.update_A_k(forward=False)

        if diff_B:
            for i in range(np.abs(diff_B)):
                if diff_B < 0:
                    self.update_B_k(forward=True)
                if diff_B > 0:
                    self.update_B_k(forward=False)

        if not (self.k_A == k_A and self.k_B == k_B):
            print('self.k does not match k from input')

        _arrI_K_B_minus_1 = self.arrI[k_B-1]
        _arrJ_K_A_minus_1 = self.arrJ[k_A-1]
        # WITH NEW UPDATED I-III_K's:
        if pullOrNot:
            _arrI_K_B_minus_1 -= 1
            _arrJ_K_A_minus_1 += 1

            # calculate for the elements that we just changed
            s_k_A_new = np.exp(-self.getWeightV(k_A-1, _arrJ_K_A_minus_1))
            s_k_B_new = np.exp(-self.getWeightH(_arrI_K_B_minus_1, k_B-1))
        else:
            _arrI_K_B_minus_1 += 1
            _arrJ_K_A_minus_1 -= 1
            s_k_A_new = np.exp(-self.getWeightV(k_A-1, _arrJ_K_A_minus_1))
            s_k_B_new = np.exp(-self.getWeightH(_arrI_K_B_minus_1, k_B-1))

        A_new = self.II_kA + self.I_kA * s_k_A_new * self.III_kA
        B_new = self.II_kB + self.I_kB * s_k_B_new * self.III_kB

        # self.A = A_new
        # self.B = B_new

        return (A_new * B_new), A_new, B_new

    def update_A_k(self, forward=True):
        if forward:
            s_k_A_plus_1 = np.exp(-self.getWeightV(self.k_A, self.arrJ[self.k_A]))
            s_k_A = np.exp(-self.getWeightV(self.k_A-1, self.arrJ[self.k_A-1]))

            self.I_kA = self.I_kA * s_k_A
            self.II_kA = self.II_kA + self.I_kA*self.mu[self.k_A]
            # self.III_kA = self.III_kA/s_k_A_plus_1 - self.mu[self.k_A]
            self.III_kA = (self.A - self.II_kA)/(self.I_kA * s_k_A_plus_1)

            self.k_A += 1
        else:
            s_k_A_minus_1 = np.exp(-self.getWeightV(self.k_A -
                                   2, self.arrJ[self.k_A-2]))
            s_k_A = np.exp(-self.getWeightV(self.k_A-1, self.arrJ[self.k_A-1]))

            # order is important:
            self.II_kA = self.II_kA - self.I_kA*self.mu[self.k_A-1]
            self.I_kA = self.I_kA / s_k_A_minus_1
            # self.III_kA = self.III_kA * s_k_A + self.mu[self.k_A-1]
            self.III_kA = (self.A - self.II_kA)/(self.I_kA * s_k_A_minus_1)

            self.k_A -= 1

    def update_B_k(self, forward=True):
        if forward:
            s_k_B_plus_1 = np.exp(-self.getWeightH(
                self.arrI[self.k_B], self.k_B))
            s_k_B = np.exp(-self.getWeightH(self.arrI[self.k_B-1], self.k_B-1))

            self.I_kB = self.I_kB * s_k_B
            self.II_kB = self.II_kB + self.I_kB * self.nu[self.k_B]
            # self.III_kB = self.III_kB/s_k_B_plus_1 - self.nu[self.k_B]
            self.III_kB = (self.B - self.II_kB)/(self.I_kB * s_k_B_plus_1)


            self.k_B += 1
        else:
            s_k_B_minus_1 = np.exp(-self.getWeightH(
                self.arrI[self.k_B-2], self.k_B-2))
            s_k_B = np.exp(-self.getWeightH(self.arrI[self.k_B-1], self.k_B-1))

            # order is important:
            self.II_kB = self.II_kB - self.I_kB*self.nu[self.k_B-1]
            self.I_kB = self.I_kB / s_k_B_minus_1
            # self.III_kB = self.III_kB * s_k_B + self.nu[self.k_B-1]
            self.III_kB = (self.B - self.II_kB)/(self.I_kB * s_k_B_minus_1)

            self.k_B -= 1

    def drawJI(self, block=False):
        # if not timeIt
        # if block:
        if False:
            plt.clf()
            nList = np.arange(self.n)
            # fig=plt.figure()
            # fig.add_subplot(aspect=1.)
            plt.title(f"Objective is {self.Obj}", fontsize='20')
            for i in range(self.m):
                mCoord = np.full(self.n, -i)
                plt.scatter(nList, mCoord, c="k", s=10/self.m)
            for i in range(self.m-1):
                plt.plot([self.arrJ[i], self.arrJ[i]], [-i, -i-1], c="r")
            for j in range(self.n-1):
                plt.plot([j, j+1], [-self.arrI[j], -self.arrI[j]], c="b")
            plt.pause(1E-30)
            if block:
                plt.show(block=True)

    def TryPushing(self):
        i = self.i
        arrJ = self.arrJ
        arrI = self.arrI
        Obj = self.Obj
        pushingPossible = False
        if (i > 0) and (i < self.m-1):  # if not in first row
            pushingPossible = (arrJ[i]-arrJ[i-1] > 0)
        elif i == 0:  # if in first row
            pushingPossible = (arrJ[i] > 0)
        if pushingPossible:
            # arrINew=arrI.copy() # arrJNew=arrJ.copy() # arrINew[arrJ[i]-1]+=1 # arrJNew[i]-=1
            k_B = arrJ[i]-1+1
            k_A = i+1
            newObj, A_new, B_new = self.recDualObj(k_A=k_A, k_B=k_B, pullOrNot=False)
            if newObj < Obj:
                self.push_succeeded+=1
                # Obj=newObj
                arrI[arrJ[i]-1] += 1
                arrJ[i] -= 1

                self.Obj = newObj
                self.arrI = arrI
                self.arrJ = arrJ
                self.A = A_new
                self.B = B_new
                return True
                # return True,arrJ,arrI,Obj
            else:
                self.push_failed+=1 
        self.push_not_possible += 1
        return False

    def TryPulling(self):
        i = self.i
        arrJ = self.arrJ
        arrI = self.arrI
        Obj = self.Obj

        pullingPossible = False
        if (i < self.m-2):  # if prior to penultimate row
            pullingPossible = (arrJ[i+1]-arrJ[i] > 0)
        elif i == self.m-2:  # if in penultimate row
            # last row is always left in col n-1
            pullingPossible = (self.n-1-arrJ[i] > 0)

        if pullingPossible:
            k_B = arrJ[i]+1
            k_A = i+1
            newObj, A_new, B_new = self.recDualObj(k_A=k_A, k_B=k_B, pullOrNot=True)
            if newObj < Obj:
                self.pull_succeeded+=1
                # Obj=newObj
                arrI[arrJ[i]] -= 1
                arrJ[i] += 1

                self.Obj = newObj
                self.arrI = arrI
                self.arrJ = arrJ
                self.A = A_new
                self.B = B_new
                return True
                # return True,arrJ,arrI,Obj
            else:
                self.pull_failed+=1 
        self.pull_not_possible += 1
        return False

    def __call__(self):
        while True:
            # print('\n\n', self.i, self.arrI, self.arrJ, self.c, '\n\n')
            didPush = False
            res = self.TryPushing()
            if res:
                self.didUpdate = True
                didPush = True
                self.drawJI()
                while res:
                    res = self.TryPushing()
            if not didPush:
                res = self.TryPulling()
                if res:
                    self.drawJI()
                    self.didUpdate = True
                    while res:
                        res = self.TryPulling()
            self.i += 1
            if self.i == self.m:
                print("\ncompleted {i} loop: ".format(i=self.i), self.Obj)
                if not self.didUpdate:
                    self.drawJI()
                    print(self.arrJ, self.arrI, self.Obj, sep='|', end='')
                    print(f"\n\n\nfinal objective is: {self.Obj} \
                        counters: ---> dim = {self.m}  ----> \
                           push_not_possible: {self.push_not_possible} \
                           push_failed: {self.push_failed}\
                           push_succeeded: {self.push_succeeded}\
                           pull_not_possible: {self.pull_not_possible}\
                           pull_failed: {self.pull_failed}\
                           pull_succeeded: {self.pull_succeeded}\
                        ")
                    return self.n, self.m,  self.arrI, self.arrJ, self.Obj
                self.didUpdate = False
                self.i = 0
                if not self.m == 4:
                    self.k_A = 1
                    self.k_B = self.arrJ[self.k_A - 1] + 1
                    print(f"\n\n K_B is: {self.k_B}\n\n")
                    _ = self.SET_DualObj(self.arrJ, self.arrI, self.m, self.n, beta=True)


def expandArrays(arrI_old, arrJ_old):
    '''input (ndarray, ndarray)'''
    arrI_old *= 2
    arrJ_old *= 2
    idx = np.where(arrJ_old == len(arrJ_old)*2)[0][0]
    arrI_old = np.repeat(arrI_old, 2)
    arrJ_old = np.repeat(arrJ_old, 2)
    arrJ_old[2*idx:] = len(arrJ_old)+1
    return np.append(arrI_old, arrI_old[-1]), np.append(arrJ_old, arrJ_old[-1])
