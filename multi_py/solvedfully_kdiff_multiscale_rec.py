#!/usr/bin/env python3 

# export the single scale solver
from solvedfully_kdiff_singlescale_lib import * 
import solvedfully_kdiff_singlescale_lib as mod

import time

from funcHeader import *

distr = int(sys.argv[1]) if len(sys.argv) > 1 else 2
params = int(sys.argv[2]) if len(sys.argv) > 2 else 1


def multiscale(mu, nu, x, y):
    '''takes finest mu/nu/x/y'''

    if len(mu) != len(x):
        print('wrong dim for mu and x not matching')

    # determine the number of scalings - i.e. log2
    dim = len(mu)
    num = int(np.log2(dim))

    print('num is ', num)

    #run loop and save as list of list, the mu's and nu's

    listOf_mus = [mu]
    listOf_nus = [nu]
    listOf_xs = [x]
    listOf_ys = [y]

    #first iteration
    dim2 = int(dim/2)
    mu_downscaled = mu.reshape(dim2,-1).sum(axis=1) #gives 512*2 output separated by row-wise [1,2], [3,4], ...
    mu_downscaled_normalized = mu_downscaled/mu_downscaled.sum() # (np.arange(10).reshape(5,-1).sum(axis=1)/np.arange(10).reshape(5,-1).sum()).sum() = 1.0
    listOf_mus.append(mu_downscaled_normalized)

    nu_downscaled = nu.reshape(dim2,-1).sum(axis=1)
    nu_downscaled_normalized = nu_downscaled/mu_downscaled.sum()
    listOf_nus.append(nu_downscaled_normalized)

    x_downscaled = x.reshape(dim2,-1).sum(axis=1)/2
    listOf_xs.append(x_downscaled)

    y_downscaled = y.reshape(dim2,-1).sum(axis=1)/2
    listOf_ys.append(y_downscaled)

    # populating the lists
    for _ in range(num-3): #start the solver with m=n=4
        dim2 = int(dim2/2)

        #take last element
        mu_downscaled = listOf_mus[-1].reshape(dim2,-1).sum(axis=1) #gives 512*2 output separated by row-wise [1,2], [3,4], ...
        mu_downscaled_normalized = mu_downscaled/mu_downscaled.sum() # (np.arange(10).reshape(5,-1).sum(axis=1)/np.arange(10).reshape(5,-1).sum()).sum() = 1.0
        listOf_mus.append(mu_downscaled_normalized)
        
        nu_downscaled = listOf_nus[-1].reshape(dim2,-1).sum(axis=1)
        nu_downscaled_normalized = nu_downscaled/nu_downscaled.sum()
        listOf_nus.append(nu_downscaled_normalized)

        x_downscaled = listOf_xs[-1].reshape(dim2,-1).sum(axis=1)/2
        listOf_xs.append(x_downscaled)

        y_downscaled = listOf_ys[-1].reshape(dim2,-1).sum(axis=1)/2
        listOf_ys.append(y_downscaled)
    
    print(f'length of mu/nu list: {len(listOf_nus), len(listOf_nus)}')
    
    # print(listOf_mus[0])
    # print('--------')
    # print(listOf_mus[1])
    # print('--------')
    # print(listOf_mus[2])
    # print('--------')
    # print(listOf_mus[0].sum())
    # print(listOf_mus[1].sum())
    # print(listOf_mus[2].sum())

    # print(listOf_xs[0])
    # print('--------')
    # print(listOf_xs[1])
    # print('--------')
    # print(listOf_xs[2])
    # print('--------')

    # now feed the mus/nus/xs/ys using the correct arrI/arrJ that I obtained from the previous iteration of the single scale solver

    #set up for m=n=4 the correct L-paths i.e. arrI/arrJ
    m=n=4
    arrJ = np.full(shape=m-1, fill_value=0, dtype=np.int32)
    arrI = np.full(shape=n-1, fill_value=m-1, dtype=np.int32)

    hk1d_Obj = mod.hk1d(arrJ=arrJ, arrI=arrI, mu=listOf_mus[-1], nu=listOf_nus[-1], x=listOf_xs[-1], y=listOf_ys[-1])
    # now __call__ the obj to run it and draw the paths
    _, _, arrI, arrJ, _ = hk1d_Obj()

    i = 2
    while True:
        arrI, arrJ = mod.expandArrays(arrI, arrJ)
        print('\narrI after expanding is ' ,arrI,'\n')
        hk1d_Obj = mod.hk1d(arrJ=arrJ, arrI=arrI, mu=listOf_mus[-i], nu=listOf_nus[-i], x=listOf_xs[-i], y=listOf_ys[-i])
        _, _, arrI, arrJ, _ = hk1d_Obj()
        i += 1
        if i > num-1:
            # hk1d_Obj.drawJI(block=True)
            print("after iterations: ", arrI, arrJ)
            listOfOutputarrIs.append(arrI)
            listOfOutputarrJs.append(arrJ)
            break

# ---------------------------------------

listOfOutputarrIs = [] 
listOfOutputarrJs = []
elapsed_time_s = []
# grep "final objective is:" for listing out final objectives 
# grep "counters: ---> " for listing the benchmark counters for failed/successful pushes/pulls

m=n=8

for _ in range(8):
    x = np.linspace(0, 1., num=m, endpoint=False)
    y = np.linspace(0, 1., num=n, endpoint=False)
    mu, nu = getDistr(distr, params,m,n, x)
    # mu = 1.5*m-np.arange(m)
    # mu = mu/np.sum(mu)
    # nu = 0.5*n+np.arange(n)  # self.nu=self.nu/np.sum(self.nu)
    # nu = nu/np.sum(nu)

    # multiscale(mu, nu, x, y)

    arrJ = np.full(shape=m-1, fill_value=0, dtype=np.int32)
    arrI = np.full(shape=n-1, fill_value=m-1, dtype=np.int32)

    start_time = time.time()

    multiscale(mu, nu, x, y)

    # hk1d_Obj = mod.hk1d(arrJ, arrI, mu, nu, x, y)

    elapsed_time_s.append(time.time() - start_time)

    # listOfOutputarrIs.append(arrI)
    # listOfOutputarrJs.append(arrJ)


    print(arrI, arrJ, sep="|")
    m*=2
    n*=2
print('list of arrIs for dim=4, 8, 16, 32, 64')
print(listOfOutputarrIs, sep = "\n")
print('---------------------------')
print('list of arrJs for dim=4, 8, 16, 32, 64')
print(listOfOutputarrJs, sep = "\n")

print("elapsed times ---> dim = 8, 16, ..., 2^9 ", elapsed_time_s)
# grep elapsed times ---> for the elapsed times


# m=n=32
# x = np.linspace(0, 1., num=m, endpoint=False)
# y = np.linspace(0, 1., num=n, endpoint=False)
# mu = 1.5*m-np.arange(m)
# mu = mu/np.sum(mu)
# nu = 0.5*n+np.arange(n)  # self.nu=self.nu/np.sum(self.nu)
# nu = nu/np.sum(nu)

# multiscale(mu, nu, x, y)
